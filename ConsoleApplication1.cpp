// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string> 
#include <vector>
#include "utils.h"
//#include "utils.cpp"
#include <fstream>
#include <cstring>
#include <istream>
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#include <map>




int main()
{
	int randomizer;
	/* initialize random seed: */
	srand(time(NULL)); 
	//chatParrot();//PARROT exec
	//std::cout << "What seems to be the problem?" << std::endl;


	//chatFixedPhrases();//FIXED PHRASES
	//chatMemoryParrot();//Read and ADD from USER
	//chatLoadPhrases();//from files
	//testRandInt();
	//mapPhrases();//map Phrases
	mapFiles();
	//char str[] = "test object well played";
	//char *ptr;
	//ptr = strtok_s(str, " ",);
	
	//ptr = strtok(NULL, " ");
	//std::cout << ptr << std::endl;
	//return 0;
}
void mapPhrases() {
	std::map <std::string, std::string> wordMap;
	std::vector <std::string> words;
	wordMap["thirsty"] = "Have a drink.";
	wordMap["tired"] = "Have more sleep.";
	wordMap["end"] = "Already? I'm not bored yet.";
	wordMap["goodbye"] = "Ok bye!";
	//std::cout << wordMap["thirsty"] << std::endl;
	wordMap["network"] = "Wait 30 minutes";
	wordMap["internet"] = "Don't bother with it";
	wordMap["broken"] = "We are all broken";
	wordMap["unresponsive"] = "?";
	std::cout << "What seems to be the problem?" << std::endl;
	while (true) {
		std::string input;
		std::getline(std::cin, input);
		words = tokenise(input, ' ');
		for (std::string word : words) {
			std::string phrase = wordMap[word];
			
			if (phrase.length() > 0) {
				std::cout << phrase << std::endl;
			}
		}
		
	}
}
void readCharFile(std::string &filePath) {
	std::ifstream in(filePath);
	char c;

	if (in.is_open()) {
		while (in.good()) {
			in.get(c);
			// Play with the data
			std::cout << c;
			
		}
	}

	if (!in.eof() && in.fail())
		std::cout << "error reading " << filePath << std::endl;

	in.close();
	
}
void mapFiles() {
	std::map <std::string, std::string> wordMap;
	std::vector <std::string> words;
	std::vector <std::string> words1;
	std::vector <std::string> words2;
	std::vector <std::string> words3;
	std::vector <std::string> words4;
	std::vector <std::string> words5;
	std::string input;
	std::vector <std::string> phrases2 = readLinesFromFile("phrases2.txt");
	words = tokenise(phrases2[0], ' ');
	words1 = tokenise(phrases2[1], ' ');
	words2 = tokenise(phrases2[2], ' ');
	words3 = tokenise(phrases2[3], ' ');
	words4 = tokenise(phrases2[4], ' ');
	words5 = tokenise(phrases2[5], ' ');

	wordMap[words[0]] = " " + words[1] + " " + words[2] + " " + words[3] + " " + words[4] + " " + words[5] + " " + words[6] + " " + words[7];
	wordMap[words1[0]] = " " + words1[1] + " " + words1[2] + " " + words1[3] + " " + words1[4] + " " + words1[5];
	wordMap[words2[0]] = " " + words2[1] + " " + words2[2] + " " + words2[3] + " " + words2[4] + " " + words2[5];
	wordMap[words3[0]] = " " + words3[1] + " " + words3[2] + " " + words3[3] + " " + words3[4] + " " + words3[5];
	wordMap[words4[0]] = " " + words4[1] + " " + words4[2] + " " + words4[3] + " " + words4[4] + " " + words4[5];
	wordMap[words5[0]] = " " + words5[1] + " " + words5[2] + " " + words5[3] + " " + words5[4] + " " + words5[5] + " " + words5[6] + " " + words5[7];
	
	std::getline(std::cin, input);// read from user
	std::vector <std::string> phrases = readLinesFromFile("phrases2.txt");
	words = tokenise(input, ' ');
	for (std::string str : words) {
		std::string phrase = wordMap[str];
		if (phrase.length() > 0) {
			std::cout << phrase << std::endl;
		}
	}	
}
void chatFixedPhrases() {
	std::vector <std::string> phrases;
	phrases.push_back("I see, tell me more...");
	phrases.push_back("I think you should calm down.");
	phrases.push_back("Come and see me in my office hour.");
	while (true) {
		int index = randInt(phrases.size());
		std::cout << phrases[index] << std::endl;
	}
	
}
void chatParrot() {
	while (true) {
		std::string input;
		std::cin >> input;
		std::cout << "parrot> " << input << std::endl;
	}
	
}
void chatMemoryParrot()
{
	std::vector <std::string> phrases;
	std::string input;
	phrases.push_back("I see, tell me more...");
	phrases.push_back("I think you should calm down.");
	phrases.push_back("Come and see me in my office hour.");
	while (true) {
		
		std::getline(std::cin, input);// read from user
		int index = randInt(phrases.size());// choose random phrase
		std::cout << phrases[index] << std::endl;// print it 
	}

}
std::vector <std::string> readLinesFromFile(std::string filename) {
	std::vector <std::string> phrases;
	std::string input;
	std::ifstream fileReader{ filename };
	while (std::getline(fileReader, input)) {
		phrases.push_back(input);
	}
	return phrases;
}
void chatLoadPhrases() {
	
	std::string input;
	std::getline(std::cin, input);// read from user
	std::vector <std::string> phrases = readLinesFromFile("phrases.txt");
	double r = std::rand();
	r /= RAND_MAX;
	r *= phrases.size();
	int i = r;
	r = rand() % phrases.size();
	
	std::cout << "numberPhrases>" << r << std::endl;
	std::cout << "FROMFILE>" << phrases[r] << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
